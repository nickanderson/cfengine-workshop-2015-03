# Additional Policy Examples
[CFEngine Content Repository](https://github.com/cfengine-content/registry) - Links to repositories that contain service specific content

[Nick's Example Infrastructure](https://github.com/nickanderson/example-a10042) - Where I play with policies and expiriment with different ways to organize policy

[Evolve Free Library](https://github.com/evolvethinking/evolve_cfengine_freelib) - Data driven (CSV, JSON) policy library

[Yale Standard Lib](https://github.com/jlgreer/yale_cfengine3/blob/master/library.cf) - Yales standard library

[CFEngine Design Center](https://github.com/cfengine/design-center/) - CFEngine policies developed for use with Design Center, can be used from cf-sketch or Mission Portal